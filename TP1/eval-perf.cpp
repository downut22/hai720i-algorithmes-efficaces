#include "eval-perf.h"
#include <stdlib.h>
#include <iostream>

#define test_function_op_count 2

void test_function(int n)
{
	int a = 5; int b = 5;
	for(int i = 0; i < n; i++)
	{
		b = a + b;
	}
}

int main()
{
	int n = 1000000000;

	EvalPerf PE;

	PE.start();
	test_function(n);
	PE.stop();

	std::cout << "Nombre de cycles : " << PE.nb_cycle()/n << "  |  " << std::endl;
	std::cout << "Duree : " << PE.milliseconds() << " ms" << std::endl;

	std::cout << "CPI : " << PE.CPI(test_function_op_count * n) << std::endl;
	std::cout << "IPC : " << PE.IPC(test_function_op_count * n) << std::endl;

	return 0;
}

