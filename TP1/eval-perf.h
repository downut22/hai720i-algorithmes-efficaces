#include <chrono>
#include <x86intrin.h>

class EvalPerf
{
	private:
		std::chrono::time_point<std::chrono::system_clock> begin;
		std::chrono::time_point<std::chrono::system_clock> end;

		uint64_t beginCycles;
		uint64_t endCycles;

		uint64_t rdtsc()
		{
			unsigned int lo,hi;
			__asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
			return ((uint64_t)hi << 32) | lo;
		}

	public:

		 void start()
		 {
		 	begin = std::chrono::system_clock::now();
		 	beginCycles=rdtsc();
		 }

	  	void stop()
		  {
		  	end = std::chrono::system_clock::now();
		  	endCycles = rdtsc();
		  }

		  uint64_t nb_cycle()
		  {
		  	return endCycles - beginCycles;
		  }

		  float CPI(int N)
	 	 {
  			return nb_cycle() / (float)N;
		  }

		  float IPC(int N)
		  {
	  			return N / (float)nb_cycle();
	 	 }

	   	int seconds()
	   {
	   		return std::chrono::duration_cast<std::chrono::seconds>(  end - begin ).count();
	  	}
	  
	   int milliseconds()
	   {
	   	return std::chrono::duration_cast<std::chrono::milliseconds>(  end - begin ).count();
	   }

  	 
  	 	int microseconds()
	   {
	   	return std::chrono::duration_cast<std::chrono::microseconds>(  end - begin ).count();
	   }

	EvalPerf()
	{
		beginCycles = 0;
		endCycles = 0;
	}
};