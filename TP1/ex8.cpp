#include "eval-perf.h"
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <vector>
#include <bits/stdc++.h> 

#define C1 0.2f
#define C2 0.3f
#define PI2 (2.0/3.0) * M_PI
#define PI4 (4.0/3.0) * M_PI

using namespace std; 

void slowperformance1(float *x, const float *y, const float *z, int n)
{
	for(int i = 0; i < n - 2; i++)
	{
		x[i] = x[i] / M_SQRT2 + y[i] * C1;
		x[i+1] = z[(i%4) * 10] * C2;
		x[i+2] = sin((2 * M_PI * i) / 3) * y[i+2];
	}
}

void slowperformance2(float *x, const float *y, const float *z, int n)
{
	for(int i = 0; i < n - 2; i++)
	{
		x[i] = x[i] / M_SQRT2 + y[i] * C1;
		//x[i+1] = z[(i%4) * 10] * C2;
		//x[i+2] = sin((2 * M_PI * i) / 3) * y[i+2];
	}
}

float mysin(int x)
{
	x %= 3;

	if(x == 0)
	{
		return 0;
	}
	else if(x == 1)
	{
		return PI2;
	}
	else //if(x == 2)
	{
		return PI4;
	}
}

void slowperformance3(float *x, const float *y, const float *z, int n)
{
	for(int i = 0; i < n - 4; i+=3)
	{
		x[i] = x[i] / M_SQRT2 + y[i] * C1;
		x[i+1] = z[(i%4) * 10] * C2;
		x[i+2] = mysin(i) * y[i+2];

		x[i+1] = x[i+1] / M_SQRT2 + y[i+1] * C1;
		x[i+2] = z[((i+1)%4) * 10] * C2;
		x[i+3] = mysin(i+1) * y[i+3];

		x[i+2] = x[i+2] / M_SQRT2 + y[i+2] * C1;
		x[i+3] = z[((i+2)%4) * 10] * C2;
		x[i+4] = mysin(i+2) * y[i+4];
	}
}

void slowperformance4(float *x, const float *y, const float *z, int n)
{
	for(int i = 0; i < n - 4; i+=3)
	{
		x[i] = x[i] / M_SQRT2 + y[i] * C1;
		x[i+1] = z[(i%4) * 10] * C2;
		x[i+2] = mysin(i) * y[i+2];

		x[i+1] = x[i+1] / M_SQRT2 + y[i+1] * C1;
		x[i+2] = z[((i+1)%4) * 10] * C2;
		x[i+3] = mysin(i+1) * y[i+3];

		x[i+2] = x[i+2] / M_SQRT2 + y[i+2] * C1;
		x[i+3] = z[((i+2)%4) * 10] * C2;
		x[i+4] = mysin(i+2) * y[i+4];
	}
}

void test(void (*function)(float*, const float*, const float*, int), vector<float> x,vector<float> y,vector<float> z, int n, int op)
{	
	EvalPerf PE;

	int count = x.size() * n;
	int opcount = count * op;

	PE.start();
	for(int i = 0; i < n;i++)
	{
		function(&x[0],&y[0],&z[0],x.size());
	}
	PE.stop();

	std::cout << "Nombre de cycles : " << PE.nb_cycle()/count << "  |  1" << std::endl;
	std::cout << "Duree : " << PE.milliseconds() << " ms" << std::endl;
	std::cout << "CPI : " << PE.CPI(opcount ) << std::endl;
	std::cout << "IPC : " << PE.IPC(opcount ) << std::endl;
}

int main()
{
	int n = 5000;

	vector<float> x,y,z; int size = 10000;
	x.resize(size); y.resize(size); z.resize(size);
	for(int i = 0; i < size;i++)
	{
		x[i] = i%100;
		y[i] = i%100;
		z[i] = i%100;
	}

	std::cout << "---   SLOW PERF 1   ---"<<std::endl;
	int opcount1 = 13;	
	test(&slowperformance1, x, y, z,n,opcount1);

	std::cout << "---   SLOW PERF 2   ---"<<std::endl;
	int opcount2 = 3;	
	test(&slowperformance2, x, y, z,n,opcount2);

	std::cout << "---   SLOW PERF 3   ---"<<std::endl;
	int opcount3 = 27;	
	test(&slowperformance3, x, y, z,n,opcount3);

	std::cout << "---   SLOW PERF 4   ---"<<std::endl;
	int opcount4 = 13;	
	//test(&slowperformance4, x, y, z,n,opcount4);

	return 0;
}

