#include "eval-perf.h"
#include <stdlib.h>
#include <iostream>

#define op_count 1

void prefix_sum(int* tab,int size)
{
	for(int i = 1; i < size;i++)
	{
		tab[i] += tab[i-1];
	}
}

void display_tab(int* tab, int size)
{
	std::cout << "TAB [size=" << size << "] ";
	for(int i = 0; i < size; i++)
	{
		std::cout << "<" << tab[i] << "> "; 
	}
	std::cout << std::endl;
}

int main()
{
	EvalPerf PE;

	int size = 1000000000; 
	int n = size;

	int * tab = new int[size];
	for(int i = 0 ; i< size;i++)
	{
		tab[i] = i;
	}

	//display_tab(tab,4);
	//prefix_sum(tab,4);
	//display_tab(tab,4);

	PE.start();
	//for(int i = 0; i < n;i++)
	//{
		prefix_sum(tab,size);
	//}
	PE.stop();

	std::cout << "Nombre de cycles par appel : " << PE.nb_cycle()/n << "  |  " << std::endl;
	std::cout << "Duree totale : " << PE.milliseconds() << " ms" << std::endl;
	std::cout << "CPI : " << PE.CPI(op_count * n) << std::endl;
	std::cout << "IPC : " << PE.IPC(op_count * n) << std::endl;

	return 0;
}

