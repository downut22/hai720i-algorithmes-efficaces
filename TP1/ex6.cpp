#include "eval-perf.h"
#include <stdlib.h>
#include <iostream>
#include <math.h>

double puissances(double x,int* polynom,int degre)
{
	double res = polynom[0];
	double last = x;
	for(int i = 1; i <= degre;i++)
	{
		res = res + (polynom[i] * last);
		last = last * x;
	}
	return res;
}

double horner(double x, int* polynom,int degre)
{
	double res = (x * polynom[degre]) + polynom[degre-1];

	degre -= 2;
	for(;degre >= 0; degre--)
	{
		res = res * x; res = res + polynom[degre];
	}

	return res;
	
}

void display_tab(int* tab, int size)
{
	std::cout << "TAB [size=" << size << "] ";
	for(int i = 0; i < size; i++)
	{
		std::cout << "<" << tab[i] << "> "; 
	}
	std::cout << std::endl;
}

int main()
{
	int n = 1000000;

	EvalPerf PE;

	int degre = 500; 
	
	int opcountp = (degre +1) * 3;
	int opcounth = (degre + 1) * 2;

	int* tab = new int[degre+1];
	for(int i = 0; i <= degre;i++)
	{
		tab[i] = i%20;
	}

	double x = 5;

	//display_tab(tab,degre);
	std::cout << "Evaluation basic for " << x << " = " << puissances(x,tab,degre) << std::endl;
	std::cout << "Evaluation Horner for " << x << " = " << horner(x,tab,degre) << std::endl;

	PE.start();
	for(int i = 0; i < n;i++)
	{
		puissances(x,tab,degre);
	}
	PE.stop();

	std::cout << "Nombre de cycles : " << PE.nb_cycle()/n << "  |  Puissances Successives" << std::endl;
	std::cout << "Duree : " << PE.milliseconds() << " ms" << std::endl;
	std::cout << "CPI : " << PE.CPI(opcountp * n) << std::endl;
	std::cout << "IPC : " << PE.IPC(opcountp * n) << std::endl;

	PE.start();
	for(int i = 0; i < n;i++)
	{
		horner(x,tab,degre);
	}
	PE.stop();

	std::cout << "Nombre de cycles : " << PE.nb_cycle()/n << "  |  Horner" << std::endl;
	std::cout << "Duree : " << PE.milliseconds() << " ms" << std::endl;
	std::cout << "CPI : " << PE.CPI(opcounth * n) << std::endl;
	std::cout << "IPC : " << PE.IPC(opcounth * n) << std::endl;


	return 0;
}

