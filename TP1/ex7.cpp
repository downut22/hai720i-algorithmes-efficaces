#include "eval-perf.h"
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <vector>
#include <bits/stdc++.h> 

using namespace std; 

#define START 1

void reduce1(vector<double> &V, double &res)
{
	res = START;
	for(size_t i = 0; i < V.size();i++)
	{
		res = res * V[i];
	}
}

void reduce2(vector<double> &V, double &res)
{
	int res1 = START; int res2 = START;

	size_t i = 0;
	for(; i < V.size();i+=2)
	{
		res1 = res1 * V[i];
		res2 = res2 * V[i+1];
	}

	if(i%2!=0)
	{
		res1 *= V[V.size()-1];
	}

	res = res1*res2;
}

void reduce3(vector<double> &V, double &res)
{
	int res1 = START; int res2 = START; int res3 = START;

	size_t i = 0;
	for(; i < V.size();i+=3)
	{
		res1 = res1 * V[i];
		res2 = res2 * V[i+1];
		res3 = res3 * V[i+2];
	}

	if(i%2!=0)
	{
		res1 *= V[V.size()-1];
	}

	res = res1*res2*res3;
}

void reduce4(vector<double> &V, double &res)
{
	int res1 = START; int res2 = START; int res3 = START; int res4 = START;

	size_t i = 0;
	for(; i < V.size();i+=4)
	{
		res1 = res1 * V[i];
		res2 = res2 * V[i+1];
		res3 = res3 * V[i+2];
		res4 = res4 + V[i+3];
	}

	if(i%2!=0)
	{
		res1 *= V[V.size()-1];
	}

	res = res1*res2*res3;
}

void display_tab(int* tab, int size)
{
	std::cout << "TAB [size=" << size << "] ";
	for(int i = 0; i < size; i++)
	{
		std::cout << "<" << tab[i] << "> "; 
	}
	std::cout << std::endl;
}

void test(void (*function)(vector<double>&, double& ), vector<double> v, int n, int op)
{	
	EvalPerf PE;

	int count = (v.size() / (float)op) * n;
	int opcount = count * op;

	double res;
	PE.start();
	for(int i = 0; i < n;i++)
	{
		function(v,res);
	}
	PE.stop();

	std::cout << "Nombre de cycles : " << PE.nb_cycle()/count << "  |  1" << std::endl;
	std::cout << "Duree : " << PE.milliseconds() << " ms" << std::endl;
	std::cout << "CPI : " << PE.CPI(opcount ) << std::endl;
	std::cout << "IPC : " << PE.IPC(opcount ) << std::endl;
}

int main()
{
	int n = 500000;

	vector<double> v; int size = 1000;
	v.resize(size); 
	for(int i = 0; i < size;i++)
	{
		v[i] = i;
	}

	std::cout << "---   REDUCE 1   ---"<<std::endl;
	int opcount1 = 1;	
	test(&reduce1,v,n,opcount1);

	std::cout << "---   REDUCE 2   ---"<<std::endl;
	int opcount2 = 2;	
	test(&reduce2,v,n,opcount2);

	std::cout << "---   REDUCE 3   ---"<<std::endl;
	int opcount3 = 3;	
	test(&reduce3,v,n,opcount3);

	std::cout << "---   REDUCE 4   ---"<<std::endl;
	int opcount4 = 4;	
	test(&reduce4,v,n,opcount4);

	return 0;
}

