#include "../TP1/eval-perf.h"
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <vector>
#include <bits/stdc++.h> 
#include<immintrin.h>
#include <algorithm>

using namespace std; 

#define START 0

__m256 x_vec, y_vec , b_vec ;

float min0(float* V, int size)
{
	float res = FLT_MAX;
	for(size_t i = 0; i < size;i++)
	{
		if(V[i] < res){res = V[i];}
	}
	return res;
}

float minalgo(float* v, int size)
{
	return *std::min_element(&v[0],&v[size-1]);
}

float min2(float* V, int size)
{
	float res = FLT_MIN;
	float temp[4];

	x_vec = _mm256_loadu_ps(V);
	for(size_t i = 4;i <= size-4;i+=4)
	{	
		y_vec = _mm256_loadu_ps(V+i);

		x_vec = _mm256_min_ps(y_vec,x_vec);
	}

	_mm256_storeu_ps(temp,x_vec);

	res = min(((temp[0] , temp[1]) , temp[2]) , temp[3]);// + temp[4] + temp[5] + temp[6] + temp[7] ;

	size_t m = size%4;
	for(size_t i = 0; i < m;i++)
	{
		res = min(res,V[size-i-1]);
	}

	return res;
}

float min1(float* V, int size)
{
	float res = FLT_MIN;
	float temp[4];

	x_vec = _mm256_loadu_ps(V);
	for(size_t i = 4;i <= size-4;i+=4)
	{	
		y_vec = _mm256_loadu_ps(V+i);

		b_vec = _mm256_cmp_ps(y_vec,x_vec,_CMP_LT_OQ);

		x_vec = _mm256_blendv_ps(x_vec,y_vec,b_vec);
	}

	_mm256_storeu_ps(temp,x_vec);

	res = min(((temp[0] , temp[1]) , temp[2]) , temp[3]);// + temp[4] + temp[5] + temp[6] + temp[7] ;

	size_t m = size%4;
	for(size_t i = 0; i < m;i++)
	{
		res = min(res,V[size-i-1]);
	}

	return res;
}

void display_tab(int* tab, int size)
{
	std::cout << "TAB [size=" << size << "] ";
	for(int i = 0; i < size; i++)
	{
		std::cout << "<" << tab[i] << "> "; 
	}
	std::cout << std::endl;
}

void test(float (*function)(float*,int ), vector<float> v, int n, int op)
{	
	EvalPerf PE;

	int opcount = v.size() * op * n;

	float res;
	PE.start();
	for(int i = 0; i < n;i++)
	{
		res = function(&v[0],v.size());
	}
	PE.stop();

	std::cout << "Resultat : " << res << std::endl;
	std::cout << "Nombre de cycles : " << PE.nb_cycle() << std::endl;///count
	std::cout << "Duree : " << PE.milliseconds() << " ms" << std::endl;
	std::cout << "CPI : " << PE.CPI(opcount ) << std::endl;
	std::cout << "IPC : " << PE.IPC(opcount ) << std::endl;
}

int main()
{
	int n = 500000;

	vector<float> v; int size = 10000;
	v.resize(size); 
	for(int i = 0; i < size;i++)
	{
		v[i] = ((i+42) * 3) % 28 + 39;
	}

	std::cout << "\n---   Algo Naif   ---"<<std::endl;
	int opcount0 = 2;
	test(&min0,v,n,opcount0);
	std::cout << "\n---   Algo SIMD COMPARE  ---"<<std::endl;
	int opcount1 = 2;	
	test(&min1,v,n,opcount1);
	std::cout << "\n---   Algo SIMD MIN  ---"<<std::endl;
	int opcount2 = 2;	
	test(&min2,v,n,opcount2);
	std::cout << "\n---   Algo std::min_element   ---"<<std::endl;
	int opcounta = 1;	
	test(&minalgo,v,n,opcounta);
	
	
	return 0;
}

