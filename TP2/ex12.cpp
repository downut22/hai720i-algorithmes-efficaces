#include "../TP1/eval-perf.h"
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <vector>
#include <bits/stdc++.h> 
#include<immintrin.h>
#include <algorithm>

using namespace std; 

#define START 0

__m256d p_vec,x_vec, y_vec,f_vec;

double puissances(double x,int* polynom,int degre)
{
	double res = polynom[0];
	double last = x;
	for(int i = 1; i <= degre;i++)
	{
		res = res + (polynom[i] * last);
		last = last * x;
	}
	return res;
}

double puissancesSIMD(double x, int* polynom, int degre)
{
	double res = 0;

	f_vec = _mm256_set_pd(1,x,x*x,x*x*x);

	p_vec = _mm256_set_pd(polynom[0],polynom[1],polynom[2],polynom[3]);

	x_vec = _mm256_set_pd(x*x*x*x,x*x*x*x,x*x*x*x,x*x*x*x);

	int i = 4; int size = degre + 1;
	for(; i < size - 3; i+=4)
	{
		y_vec = _mm256_set_pd(polynom[i],polynom[i+1],polynom[i+2],polynom[i+3]);

		p_vec = _mm256_fmadd_pd(x_vec,y_vec,p_vec);

		for(int y = 0; y < 4; y++)
		{
			x_vec = _mm256_mul_pd(x_vec,x_vec);
		}
	}

	p_vec = _mm256_mul_pd(p_vec,f_vec);

	double temp[4];
	_mm256_storeu_pd(temp,p_vec);

	res += temp[0] + temp[1] + temp[2] + temp[3];

	int remaining = size % 4;

	if( remaining != 0)
	{
		double acc = pow(x,size-remaining);

		for(i = size - remaining;i < size;i++)
		{
			res += polynom[i]  * acc;
			acc *= x;
		}
	}

	return res;
}


void display_tab(int* tab, int size)
{
	std::cout << "TAB [size=" << size << "] ";
	for(int i = 0; i < size; i++)
	{
		std::cout << "<" << tab[i] << "> "; 
	}
	std::cout << std::endl;
}

void test(double (*function)(double,int*,int ), double x, std::vector<int> poly,int n,int opcount)
{	
	EvalPerf PE;

	opcount = n * poly.size() * opcount;

	double res;
	PE.start();
	for(int i = 0; i < n;i++)
	{
		res = function(x,&poly[0],poly.size()-1);
	}
	PE.stop();

	std::cout << "Resultat : degree = "<< poly.size()-1 << "  ; x = " << x << "  >> " << res << std::endl;
	std::cout << "Nombre de cycles : " << PE.nb_cycle() << std::endl;///count
	std::cout << "Duree : " << PE.milliseconds() << " ms" << std::endl;
	std::cout << "CPI : " << PE.CPI(opcount ) << std::endl;
	std::cout << "IPC : " << PE.IPC(opcount ) << std::endl;
}

int main()
{
	int n = 10000;

	vector<int> v; int size = 9;
	v.resize(size); 
	for(int i = 0; i < size;i++)
	{
		v[i] = i;
	}

	double x = 3;

	std::cout << "\n---   Puissances Successives   ---"<<std::endl;
	int opcount0 = 3;
	test(&puissances,x,v,n,opcount0);

	std::cout << "\n---   Puissances SIMD   ---"<<std::endl;
	int opcount1 = 3;
	test(&puissancesSIMD,x,v,n,opcount1);

	return 0;
}

