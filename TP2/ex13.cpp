#include "../TP1/eval-perf.h"
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <vector>
#include <bits/stdc++.h> 
#include<immintrin.h>
#include <algorithm>
#include <iomanip>

using namespace std; 

#define START 0

struct Matrix
{
		double* tab;
		int width; int height;

		double get(int x, int y)
		{
			return tab[x + (y*width)];
		}

		void set(int x, int y, double v)
		{
			tab[x + (y*width)] = v;
		}

		void display(char* title)
		{
			std::cout << "  Matrix " << title << " : " << width << " x " << height << std::endl;
			for(int y = 0; y < height;y++)
			{
				std::cout << "  > ";
				for(int x = 0; x < width; x++)
				{
					std::cout << std::setprecision(2) << get(x,y) << " ";
				}
				std::cout << std::endl;
			}
		}

		Matrix(int w, int h)
		{
			width = w; height = h;
			tab = new double[w*h];
		}
		Matrix(){}
};

Matrix matrixProductJIK(Matrix A, Matrix B)
{
	Matrix C = Matrix(A.height,B.width);

	for(int j = 0; j < C.height; j++)
	{
		for(int i = 0; i < C.width;i++)
		{
			double acc = 0;

			for(int k = 0; k < A.width;k++)
			{
				acc += A.get(k,j) * B.get(i,k);
			}

			C.set(i,j,acc);
		}
	}

	return C;
}

Matrix matrixProductIJK(Matrix A, Matrix B)
{
	Matrix C = Matrix(A.height,B.width);

	for(int i = 0; i < C.width;i++)
	{
		for(int j = 0; j < C.height; j++)
		{
			double acc = 0;

			for(int k = 0; k < A.width;k++)
			{
				acc += A.get(k,j) * B.get(i,k);
			}

			C.set(i,j,acc);
		}
	}

	return C;
}

Matrix matrixProductKIJ(Matrix A, Matrix B)
{
	Matrix C = Matrix(A.height,B.width);

	for(int k = 0; k < A.width;k++)
	{
		for(int i = 0; i < C.width;i++)
		{
			for(int j = 0; j < C.height; j++)
			{
				C.set(i,j,C.get(i,j) + A.get(k,j) * B.get(i,k));
			}
		}
	}

	return C;
}

Matrix matrixProductKJI(Matrix A, Matrix B)
{
	Matrix C = Matrix(A.height,B.width);

	for(int k = 0; k < A.width;k++)
	{
		for(int j = 0; j < C.height; j++)
		{
			for(int i = 0; i < C.width;i++)
			{
				C.set(i,j,C.get(i,j) + A.get(k,j) * B.get(i,k));
			}
		}
	}

	return C;
}

Matrix matrixProductJKI(Matrix A, Matrix B)
{
	Matrix C = Matrix(A.height,B.width);

	for(int j = 0; j < C.height; j++)
	{
		for(int k = 0; k < A.width;k++)
		{
			for(int i = 0; i < C.width;i++)
			{
				C.set(i,j,C.get(i,j) + A.get(k,j) * B.get(i,k));
			}
		}
	}

	return C;
}

Matrix matrixProductIKJ(Matrix A, Matrix B)
{
	Matrix C = Matrix(A.height,B.width);

	for(int i = 0; i < C.width;i++)
	{
		for(int k = 0; k < A.width;k++)
		{
			for(int j = 0; j < C.height; j++)
			{
				C.set(i,j,C.get(i,j) + A.get(k,j) * B.get(i,k));
			}
		}
	}

	return C;
}

__m256d x_vec, y_vec , z_vec;

Matrix matrixProductSIMD(Matrix A, Matrix B)
{
	Matrix C = Matrix(A.height,B.width);

	double temp[4];

	for(int j = 0 ; j < C.height;j++)
	{
		for(int i = 0; i < C.width; i++)
		{
			double res = 0; double temp[4];

			x_vec = _mm256_set_pd(0,0,0,0);//A.get(i,0),A.get(i,1),A.get(i,2),A.get(i,3));

			int k = 0;
			for(; k <= A.width-4;k+=4)
			{
				y_vec = _mm256_set_pd(A.get(i,k),A.get(i,k+1),A.get(i,k+2),A.get(i,k+3));

				z_vec = _mm256_set_pd(B.get(k,j),B.get(k+1,j),B.get(k+2,j),B.get(k+3,j));

				x_vec = _mm256_fmadd_pd(y_vec,z_vec,x_vec);
			}

			_mm256_storeu_pd(temp,x_vec);

			res += temp[0] + temp[1] + temp[2] + temp[3];

			int remaining = A.width % 4;
			if( remaining != 0)
			{
				for(k = A.width - remaining;k<A.width;k++)
				{
					res += A.get(i,k) * B.get(k,j);
				}
			}

			C.set(i,j,res);
		}
	}

/*
	for(int j = 0; j < C.height; j++)
	{
		for(int k = 0; k < A.width;k++)
		{
			for(int i = 0; i <= C.width-4;i+=4)
			{
				f_vec = _mm256_set_pd(A.get(k,j),A.get(k,j),A.get(k,j),A.get(k,j));
				m_vec = _mm256_set_pd(B.get(i,k),B.get(i+1,k),B.get(i+2,k),B.get(i+3,k));

				a_vec = _mm256_set_pd(C.get(i,j),C.get(i+1,j),C.get(i+2,j),C.get(i+3,j));

				r_vec = _mm256_fmadd_pd(f_vec,m_vec,a_vec);

				_mm256_storeu_pd(temp,r_vec);

				C.set(i,j,temp[0]);
				C.set(i+1,j,temp[1]);
				C.set(i+2,j,temp[2]);
				C.set(i+3,j,temp[3]);
			}

			int rest = C.width % 4;
			for(;rest > 0;rest--)
			{
				C.set(C.width-rest,j,A.get(k,j) * B.get(C.width-rest,k) + C.get(C.width-rest,j));
			}
		}
	}*/

	return C;
}

void test(Matrix (*function)(Matrix,Matrix), Matrix A, Matrix B,int n,int opcount)
{	
	EvalPerf PE;

	opcount *= A.width * B.height * A.height * n;

	Matrix res;

	PE.start();
	for(int i = 0; i < n;i++)
	{
		res = function(A,B);
	}
	PE.stop();

	//res.display("C");
	std::cout << "Nombre de cycles : " << PE.nb_cycle() << std::endl;///count
	std::cout << "Duree : " << PE.milliseconds() << " ms" << std::endl;
	std::cout << "CPI : " << PE.CPI(opcount ) << std::endl;
	std::cout << "IPC : " << PE.IPC(opcount ) << std::endl;
}

// 2  2  2
// 6  2  6
// 3  0  12

int main()
{
	int n = 1000;

	Matrix A = Matrix(100,100);
	Matrix B = Matrix(100,100);

	/*
	A.set(0,0,1.0);A.set(1,0,0.0);A.set(2,0,0.0);
	A.set(0,1,0.0);A.set(1,1,2.0);A.set(2,1,0.0);
	A.set(0,2,0.0);A.set(1,2,0.0);A.set(2,2,3.0);

	B.set(0,0,2.0);B.set(1,0,2.0);B.set(2,0,2.0);
	B.set(0,1,3.0);B.set(1,1,1.0);B.set(2,1,3.0);
	B.set(0,2,1.0);B.set(1,2,0.0);B.set(2,2,4.0);*/
	
	for(int i = 0; i < A.width * A.height;i++)
	{
		A.tab[i] = i % 3;//(i * 5) % 59;
		B.tab[i] = i % 3;//(i * 70) % 78;
	}

	//A.display("A");
	//B.display("B");

	std::cout << "\n---   Matrix Product JIK   ---------------------------------------"<<std::endl;
	int opcount1 = 2;
	test(&matrixProductJIK,A,B,n,opcount1);

	std::cout << "\n---   Matrix Product IJK   ---------------------------------------"<<std::endl;
	int opcount2 = 2;
	test(&matrixProductIJK,A,B,n,opcount2);

	std::cout << "\n---   Matrix Product KIJ   ---------------------------------------"<<std::endl;
	int opcount3 = 2;
	test(&matrixProductKIJ,A,B,n,opcount3);

	std::cout << "\n---   Matrix Product KJI   ---------------------------------------"<<std::endl;
	int opcount4 = 2;
	test(&matrixProductKJI,A,B,n,opcount4);

	std::cout << "\n---   Matrix Product IKJ   ---------------------------------------"<<std::endl;
	int opcount6 = 2;
	test(&matrixProductIKJ,A,B,n,opcount6);

	std::cout << "\n\n---   Meilleure solution : Matrix Product JKI ---------------------------------------"<<std::endl;
	int opcount5 = 2;
	test(&matrixProductJKI,A,B,n,opcount5);

		std::cout << "\n\n---   Matrix Product JIK SIMD ---------------------------------------"<<std::endl;
	int opcount7 = 2;
	test(&matrixProductSIMD,A,B,n,opcount7);

	return 0;
}

