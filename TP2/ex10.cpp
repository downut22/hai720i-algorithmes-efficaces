#include "../TP1/eval-perf.h"
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <vector>
#include <bits/stdc++.h> 
#include<immintrin.h>
#include <x86intrin.h>

using namespace std; 

#define START 1

__m256d x_vec, y_vec ;

double reduce0(double* V, int size)
{
	double res = START;
	for(size_t i = 0; i < size;i++)
	{
		res = res * V[i];
	}
	return res;
}

double reduce1(double* V, int size)
{
	double res = START;
	double temp[4];

	x_vec = _mm256_loadu_pd(V);

	for(size_t i = 4;i <= size-4;i+=4)
	{
		y_vec = _mm256_loadu_pd(V+i);

		x_vec = _mm256_mul_pd(x_vec,y_vec);
	}

	_mm256_storeu_pd(temp,x_vec);

	res *= temp[0] * temp[1] * temp[2] * temp[3];

	size_t m = size%4;
	for(size_t i = 0; i < m;i++)
	{
		res *= V[size-i-1];
	}

	return res;
}

void display_tab(int* tab, int size)
{
	std::cout << "TAB [size=" << size << "] ";
	for(int i = 0; i < size; i++)
	{
		std::cout << "<" << tab[i] << "> "; 
	}
	std::cout << std::endl;
}

void test(double (*function)(double*,int ), vector<double> v, int n, int op)
{	
	EvalPerf PE;

	uint64_t opcount = v.size() * op * n;

	double res;
	PE.start();
	for(int i = 0; i < n;i++)
	{
		res = function(&v[0],v.size());
	}
	PE.stop();

	std::cout << "Resultat : " << res << "  |" << std::endl;
	std::cout << "Nombre de cycles : " << PE.nb_cycle() << "  |  1" << std::endl;///count
	std::cout << "Duree : " << PE.milliseconds() << " ms" << std::endl;
	std::cout << "CPI : " << PE.CPI(opcount ) << std::endl;
	std::cout << "IPC : " << PE.IPC(opcount ) << std::endl;
}

int main()
{
	int n = 500000;

	vector<double> v; int size = 10000;
	v.resize(size); 
	for(int i = 0; i < size;i++)
	{
		v[i] = 2;
	}

	std::cout << "---   REDUCE 0   ---"<<std::endl;
	int opcount0 = 1;
	test(&reduce0,v,n,opcount0);
	std::cout << "---   REDUCE 1   ---"<<std::endl;
	int opcount1 = 1;	
	test(&reduce1,v,n,opcount1);
	/*
	std::cout << "---   REDUCE 2   ---"<<std::endl;
	int opcount2 = 2;	
	test(&reduce2,v,n,opcount2);

	std::cout << "---   REDUCE 3   ---"<<std::endl;
	int opcount3 = 3;	
	test(&reduce3,v,n,opcount3);

	std::cout << "---   REDUCE 4   ---"<<std::endl;
	int opcount4 = 4;	
	test(&reduce4,v,n,opcount4);
	*/
	
	return 0;
}

