#include "../TP1/eval-perf.h"
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <vector>
#include <bits/stdc++.h> 
#include<immintrin.h>
#include <algorithm>
#include <iomanip>

using namespace std; 

#define START 0

struct Matrix
{
		int64_t* tab;
		int width; int height;

		int64_t get(int x, int y)
		{
			return tab[x + (y*width)];
		}

		void set(int x, int y, int64_t v)
		{
			tab[x + (y*width)] = v;
		}

		void display(char* title)
		{
			std::cout << "  Matrix " << title << " : " << width << " x " << height << std::endl;
			for(int y = 0; y < height;y++)
			{
				std::cout << "  > ";
				for(int x = 0; x < width; x++)
				{
					std::cout << std::setprecision(2) << get(x,y) << " ";
				}
				std::cout << std::endl;
			}
		}

		Matrix(int w, int h)
		{
			width = w; height = h;
			tab = new int64_t[w*h];
		}
		Matrix(){}
};

void transposeBasic(Matrix A)
{
	for(int i = 0; i < A.width;i++)
	{
		for(int j = i+1; j < A.height;j++)
		{
			int64_t a = A.get(j,i);
			int64_t b = A.get(i,j);
			A.set(i,j,a);
			A.set(j,i,b);
		}
	}
}

void test(void (*function)(Matrix), Matrix A, int n,int opcount)
{	
	EvalPerf PE;

	opcount *= A.width * A.height * n;

	PE.start();
	for(int i = 0; i < n;i++)
	{
		function(A);
	}
	PE.stop();

	A.display("At");
	std::cout << "Nombre de cycles : " << PE.nb_cycle() << std::endl;///count
	std::cout << "Duree : " << PE.milliseconds() << " ms" << std::endl;
	std::cout << "CPI : " << PE.CPI(opcount ) << std::endl;
	std::cout << "IPC : " << PE.IPC(opcount ) << std::endl;
}

// 2  2  2
// 6  2  6
// 3  0  12

int main()
{
	int n = 1001;

	Matrix A = Matrix(4,4);
	
	for(int i = 0; i < A.width * A.height;i++)
	{
		A.tab[i] = i;//(i * 5) % 59;
	}

	A.display("A");

	std::cout << "\n---   Matrix Transpose basic   ---------------------------------------"<<std::endl;
	int opcount1 = 2;
	test(&transposeBasic,A,n,opcount1);

	return 0;
}

