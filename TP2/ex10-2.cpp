#include "../TP1/eval-perf.h"
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <vector>
#include <bits/stdc++.h> 
#include<immintrin.h>

using namespace std; 

#define START 0

__m256i x_vec, y_vec ;

int32_t reduce0(int32_t* V, int size)
{
	int32_t res = START;
	for(size_t i = 0; i < size;i++)
	{
		res = res + V[i];
	}
	return res;
}

int32_t reduce1(int32_t* V, int size)
{
	int32_t res = START;
	int32_t temp[8];

	x_vec = _mm256_loadu_si256((__m256i*) V);

	for(size_t i = 8;i <= size-8;i+=8)
	{
		y_vec = _mm256_loadu_si256((__m256i*)(V+i));

		x_vec = _mm256_add_epi32(x_vec,y_vec);
	}

	_mm256_storeu_si256((__m256i*)temp,x_vec);

	res += temp[0] + temp[1] + temp[2] + temp[3] + temp[4] + temp[5] + temp[6] + temp[7] ;

	size_t m = size%8;
	for(size_t i = 0; i < m;i++)
	{
		res += V[size-i-1];
	}

	return res;
}

void display_tab(int* tab, int size)
{
	std::cout << "TAB [size=" << size << "] ";
	for(int i = 0; i < size; i++)
	{
		std::cout << "<" << tab[i] << "> "; 
	}
	std::cout << std::endl;
}

void test(int32_t (*function)(int32_t*,int ), vector<int32_t> v, int n, int op)
{	
	EvalPerf PE;

	int count = (v.size() / (float)op) * n;
	int opcount = count * op;

	int32_t res;
	PE.start();
	for(int i = 0; i < n;i++)
	{
		res = function(&v[0],v.size());
	}
	PE.stop();

	std::cout << "Resultat : " << res << "  |" << std::endl;
	std::cout << "Nombre de cycles : " << PE.nb_cycle() << "  |  1" << std::endl;///count
	std::cout << "Duree : " << PE.milliseconds() << " ms" << std::endl;
	std::cout << "CPI : " << PE.CPI(opcount ) << std::endl;
	std::cout << "IPC : " << PE.IPC(opcount ) << std::endl;
}

int main()
{
	int n = 500000;

	vector<int32_t> v; int size = 1000;
	v.resize(size); 
	for(int i = 0; i < size;i++)
	{
		v[i] = 2;
	}

	std::cout << "---   REDUCE 0   ---"<<std::endl;
	int opcount0 = 1;
	test(&reduce0,v,n,opcount0);
	std::cout << "---   REDUCE 1   ---"<<std::endl;
	int opcount1 = 1;	
	test(&reduce1,v,n,opcount1);
	/*
	std::cout << "---   REDUCE 2   ---"<<std::endl;
	int opcount2 = 2;	
	test(&reduce2,v,n,opcount2);

	std::cout << "---   REDUCE 3   ---"<<std::endl;
	int opcount3 = 3;	
	test(&reduce3,v,n,opcount3);

	std::cout << "---   REDUCE 4   ---"<<std::endl;
	int opcount4 = 4;	
	test(&reduce4,v,n,opcount4);
	*/
	
	return 0;
}

