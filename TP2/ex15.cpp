#include "../TP1/eval-perf.h"
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <vector>
#include <bits/stdc++.h> 
#include<immintrin.h>
#include <algorithm>
#include <iomanip>

using namespace std; 

template <typename T>
void triN(vector<T>& arr)
{
	for(int i = 0; i < arr.size();i++)
	{
		for(int k = i+1; k < arr.size();k++)
		{
			if(arr[i] > arr[k])
			{
				double a = arr[i]; double b = arr[k];
				arr[i] = b; arr[k] = a;
			}
		}
	}
}

template <typename T>
void triLog(vector<T>& arr)
{
}

template <typename T>
void display_tab(vector<T> tab)
{
	std::cout << "TAB [size=" << tab.size() << "] ";
	for(int i = 0; i < tab.size(); i++)
	{
		std::cout << "<" << (int)tab[i] << "> ";
	}
	std::cout << std::endl;
}

template <typename T>
void test(void (*function)(vector<T>&), vector<T> v,int n,int opcount, char* str)
{	
	EvalPerf PE;

	opcount *= n * v.size();

	vector<T> res = vector<T>(v);
	PE.start();
	for(int i = 0; i < n;i++)
	{
		function(res);
	}
	PE.stop();

	std::cout << str << std::endl;
	display_tab(res);
	std::cout << "Nombre de cycles : " << PE.nb_cycle() << std::endl;///count
	std::cout << "Duree : " << PE.milliseconds() << " ms" << std::endl;
	std::cout << "CPI : " << PE.CPI(opcount ) << std::endl;
	std::cout << "IPC : " << PE.IPC(opcount ) << std::endl;
}

int main()
{
	int n = 1;

	int size = 10;
	vector<int8_t> V8; V8.resize(size) ;
	for(int8_t i = 0; i < size; i++)
	{
		V8[i] = (((i+5) % 12) * 7) % 20;
	}
	vector<int16_t> V16; V16.resize(size) ;
	for(int16_t i = 0; i < size; i++)
	{
		V16[i] = (((i+5) % 12) * 7) % 20;
	}
	vector<int32_t> V32; V32.resize(size) ;
	for(int32_t i = 0; i < size; i++)
	{
		V32[i] = (((i+5) % 12) * 7) % 20;
	}
	vector<int64_t> V64; V64.resize(size) ;
	for(int64_t i = 0; i < size; i++)
	{
		V64[i] = (((i+5) % 12) * 7) % 20;
	}
	display_tab(V8);

	std::cout << "\n\n---   Tri N2 ---------------------------------------"<<std::endl;
	int opcount1 = 1;
	test(&triN,V8,n,opcount1,"8 bits");
	test(&triN,V16,n,opcount1, "16 bits");
	test(&triN,V32,n,opcount1, "32 bits");
	test(&triN,V64,n,opcount1, "64 bits");

	std::cout << "\n\n---   Tri NLOGN ---------------------------------------"<<std::endl;
	int opcount2 = 1;
	test(&triLog,V8,n,opcount2,"8 bits");
	test(&triLog,V16,n,opcount2, "16 bits");
	test(&triLog,V32,n,opcount2, "32 bits");
	test(&triLog,V64,n,opcount2, "64 bits");

	return 0;
}

