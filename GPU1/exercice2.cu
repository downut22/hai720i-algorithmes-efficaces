#include <cstdio>
#include <cstdlib>
#include <stdlib.h>
#include <iostream>
#include <vector>


void addVector(std::vector<float> A, std::vector<float> B, std::vector<float>&C)
{
    for(int i = 0 ;i < A.size();i++)
    {
      C[i] = A[i] + B[i];
    }
}

int main(int argc, char** argv) {
  
  std::vector<float> vectorA;
  std::vector<float> vectorB;
  std::vector<float> vectorC;

  int n = 1000;

  vectorA.resize(n); vectorB.resize(n); vectorC.resize(n);
  for(int i = 0 ; i < n ; i++){vectorA[i] = i; vectorB[i] = i;}

  addVector(vectorA,vectorB,vectorC);

  for(int i = 0; i < 20;i++)
  {
    std::cout<<vectorC[i]<<" | ";
  }
  std::cout<<std::endl;

  return 0;
}
