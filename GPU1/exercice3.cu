#include <cstdio>
#include <cstdlib>
#include <stdlib.h>
#include <iostream>

#include "image-pnm.h"

__device__ uint8_t max(uint8_t a, uint8_t b)
{
  return a > b ? a : b;
}

__device__ uint8_t min(uint8_t a, uint8_t b)
{
  return a < b ? a : b;
}

__global__ void blur_kernel(int *imgout,int *imgin, int width,int height,int blurSize)
{
  int y = blockIdx.y*blockDim.y + threadIdx.y;
  int x = blockIdx.x*blockDim.x + threadIdx.x;

  if(x > width ||y > height){return;}

  int minx = max(0,x-blurSize);int miny= max(0,y-blurSize);
  int maxx = min(width-1,x+blurSize);int maxy= min(height-1,y+blurSize);

  int acc = 0; int count = 0;
  for(int px = minx; px <= maxx;px++)
  {
    for(int py = miny; py <= maxy; py++)
    {
      acc += imgin[px+py*width]; count++;
    }
  }

  imgout[x+y*width] = acc/count;
}

void blur(Image<int>& inputImg,Image<int>& ouputImg,int blurSize)
{
    int w = inputImg.width(); int h = inputImg.height();

    for(int x = 0; x < w; x++)
    {
      for(int y = 0; y < h; y++)
      {
          int minx = std::max(0,x-blurSize);int miny= std::max(0,y-blurSize);
          int maxx = std::min(w-1,x+blurSize);int maxy= std::min(h-1,y+blurSize);

          int acc = 0; int count = 0;
          for(int px = minx; px <= maxx;px++)
          {
            for(int py = miny; py <= maxy; py++)
            {
              acc += inputImg.get(px,py); count++;
            }
          }

          ouputImg.set(x,y,acc/count);
      }
    }
}

int main(int argc, char** argv) {
  
  if(argc < 5){
  	std::cout << "Error : no input type, input file, ouput file or size" << std::endl;
  }

  char* type = argv[1];
  char* input = argv[2];
  char* output = argv[3];
  int size = std::atoi(argv[4]);

  std::cout<<"Input file : " << input <<std::endl;
  std::cout<<"Output file : " << output << std::endl;

  Image<int> inputImg = Image<int>(type,0,0);
  inputImg.read(input);

  Image<int> ouputImg = Image<int>(type,inputImg.width(),inputImg.height());

  //blur(inputImg,ouputImg,size);

  dim3 DimGrid( (((inputImg.width()-1)/16)+1) , (((inputImg.height()-1)/16)+1) );
  dim3 DimBlock(16,16,1);
  blur_kernel<<<DimGrid,DimBlock>>>(inputImg.getData(),ouputImg.getData(),inputImg.width(),inputImg.height(),size);

  ouputImg.write(output);

  std::cout<<"Finished"<<std::endl;

  return 0;
}
