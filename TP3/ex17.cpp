#include "../TP1/eval-perf.h"
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <vector>
#include <bits/stdc++.h> 
#include<immintrin.h>
#include <algorithm>
#include <iomanip>

using namespace std; 

#define START 0


void test(void (*function)(size_t,const double*,double*,double*,size_t), int size, double* A, double* V, int k, int n,int opcount)
{	
	EvalPerf PE;

	opcount *= size*size*n;

	double* temp = new double[size];

	PE.start();
	for(int i = 0; i < n;i++)
	{
		function(size,A,V,temp,k);
	}
	PE.stop();

	std::cout << "Nombre de cycles : " << PE.nb_cycle() << std::endl;///count
	std::cout << "Duree : " << PE.milliseconds() << " ms" << std::endl;
	std::cout << "CPI : " << PE.CPI(opcount ) << std::endl;
	std::cout << "IPC : " << PE.IPC(opcount ) << std::endl;
}

int main()
{
	int n = 10000;

	double* A = new double[size*size];
	double* V = new double[size];

	for(int i = 0; i < size;i++)
		{		
			V[i] = i;
		}
		for(int i = 0; i < size*size;i++)
		{
			A[i] = i;
		}

		std::cout << "\n     ---   MAT 2 VEC   < k = " << k << " >   ---------------------------------------"<<std::endl;
		int opcount1 = 2;
		test(&mat2vec,size,A,V,k,n,opcount1);
	}

	return 0;
}





//Question 1 --------------------------------------------------------------------------------------------------------------------------------------
//	W(n) = n^3

//Question 2 --------------------------------------------------------------------------------------------------------------------------------------
//	Nombre d'octet par double : 8
//	


