#include "../TP1/eval-perf.h"
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <vector>
#include <bits/stdc++.h> 
#include<immintrin.h>
#include <algorithm>
#include <iomanip>

using namespace std; 

#define START 0

void mat2vec(size_t n, const double* A, double* y, double* V,double* temp, size_t k)
{
	for(int i = 0 ; i < n ; ++i)
	{
		for(int j = 0; j < n; j+=k)
		{
			temp[i] += A[i*n + j] * y[j];
		}		
	}

	for(int i = 0 ; i < n ; ++i)
	{
		for(int j = 0; j < n; j+=k)
		{
			y[i] += A[i*n + j] * temp[j];
		}
	}
}

void mat2tvec(size_t n, const double* A, double* y,double* V, double* temp, size_t k)
{
	for(int i = 0 ; i < n ; ++i)
	{
		for(int j = 0; j < n; j+=k)
		{
			temp[i] += A[j*n + i] * V[j];
		}		
	}

	for(int i = 0 ; i < n ; ++i)
	{
		for(int j = 0; j < n; j+=k)
		{
			y[i] += A[i*n + j] * temp[j];
		}
	}
}

void test(void (*function)(size_t,const double*,double*,double*,double*,size_t), int size, double* A, double* V, int k, int n,int opcount)
{	
	EvalPerf PE;

	opcount *= size*size*n;

	double* temp = new double[size];
	double* res =  new double[size];

	PE.start();
	for(int i = 0; i < n;i++)
	{
		function(size,A,res,V,temp,k);
	}
	PE.stop();

	//std::cout << "Nombre de cycles : " << PE.nb_cycle() << std::endl;///count
	std::cout << "Duree : " << PE.milliseconds() << " ms" << "   CPI : " << PE.CPI(opcount ) << "   IPC : " << PE.IPC(opcount ) << std::endl;
}

int main()
{
	int n = 10000;

	int* T = new int[3]{4,6,8};
	int* K = new int[3]{1,7,16};

	for(int j = 0; j < 3;j++)
	{
		int k = K[j];
	for(int i = 0; i < 3;i++)
	{
		int t = T[i];

		std::cout << "\nT = " << t << "  K = " << k <<std::endl;
		int size = 1;
		size<<=t;//pow(2,t);

		double* A = new double[size*size];
		double* V = new double[size];

		for(int i = 0; i < size;i++)
		{		
			V[i] = i;
		}
		for(int i = 0; i < size*size;i++)
		{
			A[i] = i;
		}

		std::cout << "\n     ---   MAT 2 VEC   < k = " << k << " >   ---------------------------------------"<<std::endl;
		int opcount1 = 2;
		test(&mat2vec,size,A,V,k,n,opcount1);

		std::cout << "     ---   MAT 2t VEC   < k = " << k << " >   ---------------------------------------"<<std::endl;
		int opcount2 = 2;
		test(&mat2tvec,size,A,V,k,n,opcount2);
	}
	}

	return 0;
}





//Question 1 --------------------------------------------------------------------------------------------------------------------------------------
//	W(n) = n^3

//Question 2 --------------------------------------------------------------------------------------------------------------------------------------
//	Nombre d'octet par double : 8
//	


